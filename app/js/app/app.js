angular.module("TravelDeals", ['ngMaterial', 'ngAnimate', 'ngMessages', 'ngAria'])
.config(function($locationProvider, $mdThemingProvider, $mdDateLocaleProvider, $httpProvider){
	$locationProvider.html5Mode(true);
	$mdThemingProvider.theme('default')
    	.primaryPalette('green')
    	.accentPalette('amber')
    	.warnPalette('red');

    $mdDateLocaleProvider.formatDate = function(date) {
		return date ? moment(date).format('DD-MM-YYYY') : '';
	};
});
