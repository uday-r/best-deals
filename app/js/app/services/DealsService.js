angular.module('TravelDeals')
  .factory('DealsService', function($http, $q) {
    var deals = [];
    var desiredPaths = [];
    var currentPath = [];
    var visitedCities = [];

    var getAllPathsFrom = function(source) {
      return _.filter(deals, {'departure': source});
    };

    var checkIfDestinationVisited = function(path) {
      if(currentPath.length > 0 && 
          (path.arrival == currentPath[0].departure || path.arrival == currentPath[0].arrival)) {
        return true;
      }

      if(visitedCities.length > 0 && visitedCities.indexOf(path.arrival) > -1) {
        return true;
      }

      var sourceIndex = _.findIndex(currentPath, {'arrival': path.departure});
      var destinationIndex = _.findIndex(currentPath, {'arrival': path.arrival});

      if(destinationIndex == -1) {
        return false;
      }

      return (sourceIndex > destinationIndex)? true: false;
    }

    var checkIfDestinationCanBeReached = function(departure, arrival) {
      var paths = getAllPathsFrom(departure);
      var destFound = _.findIndex(paths, {'arrival': arrival});
      return destFound == -1? false: true;
    }

    var getDeal = function(travelHops) {
      var price = 0;
      var hours = 0;
      var minutes = 0;

      travelHops.forEach(function(hop) {
        price = price + hop.cost - hop.discount;
        hours = hours + parseInt(hop.duration.h);
        minutes = minutes + parseInt(hop.duration.m);
      });

      hours = hours + minutes/60;

      return {
        'hops': travelHops,
        'price': price,
        'hours': hours
      }
    }

    var getPaths = function(source, destination) {
      var pathsFromSource = getAllPathsFrom(source);
      var currentPathClone = [];

      pathsFromSource.forEach(function(path) {
        if(destination == path.arrival) {
          currentPath.push(path);
          currentPathClone = [];
          angular.copy(currentPath, currentPathClone);
          desiredPaths.push(getDeal(currentPathClone));
          currentPath.pop();
        } else if (!checkIfDestinationVisited(path)) {
          currentPath.push(path);
          getPaths(path.arrival, destination);
          currentPath.pop();
          if(!checkIfDestinationCanBeReached(path.arrival, destination)) {
            visitedCities.push(path.arrival);
          }
        }
      });

      return desiredPaths;
    }

    return {
      loadAllDeals: function() {
        var defer = $q.defer();
        $http.get("/response.json")
          .then(function(response) {
            deals = response.data.deals;
            defer.resolve();
          });
        return defer.promise;
      },

      getAllDestination: function() {
        var destinations = [];
        deals.forEach(function(deal) { 
          if(destinations.indexOf(deal.arrival) == -1) { 
            destinations.push(deal.arrival);
          }
        });
        return destinations;
      },

      getAllDealsBetween: function(source, destination) {
        desiredPaths = [];
        visitedCities = [];
        return getPaths(source, destination);
      }

    }
});
