angular.module('TravelDeals')
.directive('timeline', function($rootScope, $timeout) {
  return {
    templateUrl: '/js/app/directives/templates/timeline.html',
    transclude: true,
    scope: {
      items: '='
    },
    link: {
        post: function(scope, el, attrs, ctrl) {
          scope.timelineBar = {show: false};
          $timeout(function(params){
            var timelineItems = params.element.find('.time-item > button');
            params.$scope.timelineBar.width = $(timelineItems[timelineItems.length - 1]).position().left - $(timelineItems[0]).position().left;
            params.$scope.timelineBar.left = Math.ceil($(timelineItems[0]).position().left);
            params.$scope.timelineBar.show = true;
          }, 0, true, {element: el, $scope: scope});
        }
    },
    controller: function($scope) {
    }
  }
});
