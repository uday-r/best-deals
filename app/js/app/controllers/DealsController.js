angular.module('TravelDeals')
.controller('DealsController'
    , ['$scope', '$timeout', 'DealsService', '$element', function($scope, $timeout, DealsService, $element) {

  $scope.transportVehicle = {
    'car': 'directions_car',
    'train': 'train',
    'bus': 'directions_bus'
  }

  $scope.deals = [];
  $scope.cheapestDeal = {};

  $scope.searchSource;

  $element.find('input').on('keydown', function(ev) {
    ev.stopPropagation();
  });

  $scope.getLengthInfo = function() {
    if($scope.deals.length == 0) {
      return;
    }
    return "(" + $scope.deals.length + ")";
  }

  $scope.searchDeals = function() {
    $scope.deals = DealsService.getAllDealsBetween($scope.sourceStation, $scope.destinationStation);
    $scope.cheapestDeal = _.sortBy($scope.deals, 'price')[0];
    $scope.fastestDeal = _.sortBy($scope.deals, 'hours')[0];
    $scope.source = $scope.sourceStation;
    $scope.destination = $scope.destinationStation;
  }

  DealsService.loadAllDeals()
    .then(function() {
      $scope.stations = DealsService.getAllDestination();
    });
}]);
