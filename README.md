# Best deals #

Lists all best deals w.r.t price and time.

### Technologies used ###

* Node, npm for server.
* Angular.js(1.5.8), angular-material(1.1.0) all UI done using material-angular.
* Sorting and most operations done using lodash.

### Setup ###

* Download the repo.
* Assuming npm and node are installed, run "npm install".
* Open up your browser and hit "http://localhost:8089/"
* Live [Demo](https://best-dealss.herokuapp.com)

### Highlight ###

* Contains a directive(js/app/directive/timeline.js) for horizontal timeline, it still has to be made more elegant by adding support for responsiveness. Its more of a POC, stay tuned for completed version.